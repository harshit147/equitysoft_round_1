<?php
session_start();
class Main {

   public function login($user = array(),$connection){
      $response =array();
      $username = $user["username"];
      $password = $user["password"];
      $query_string = "SELECT email,password from users WHERE email = '$username' AND password = '$password'";    
      $login_query = mysqli_query($connection,$query_string);
      if(mysqli_num_rows($login_query)>=1)
      {
         $fetch_data = mysqli_query($connection,"SELECT * FROM users WHERE email = '$username'");
         $data = mysqli_fetch_assoc($fetch_data);
         $full_name = $data["name"];
         $_SESSION["username"] = $username;
         $_SESSION["full_name"] = $full_name;
         $response["error"]   = false;
         $response["status"]  = "Success";
         $response["message"] = "Successful login";  
      }
      else{
         $response["error"]   = true;
         $response["status"]  = "Fail";
         $response["message"] = "Login Failed due to credentials error";
      }
      $json_response = json_encode($response);
      return $json_response;
   }
   public function signup($user_data , $connection)
   {
      $response = array();
      $full_name = $user_data["full_name"];
      $username  = $user_data["username"];
      $password  = $user_data["password"];
      /**
       * Check If User is already existed
      */
      $check_email = mysqli_query($connection,"SELECT id FROM users WHERE email = '$username'");
      if(mysqli_num_rows($check_email)>=1){
         $response["error"]   = true;
         $response["message"] = "User Already Exists";
         $response["status"]  = "Fail";
      }
      else{
         $creating = mysqli_query($connection,"INSERT INTO users(name,email,password) VALUES('$full_name','$username','$password')");
         if(mysqli_affected_rows($connection) >=1){
            $response["error"]   = false;
            $response["message"] = "Created Successfully";
            $response["status"] = "Success";
         }
      }
      $json_response = json_encode($response);
      return $json_response;
   }
   // public function remove($data , $connection_string){
   //    $response = array();
   //    $row_id = $data["row_id"];
   //    $check_credentials = mysqli_query($connection_string,"SELECT * FROM users WHERE id = '$row_id'");
   //    if(mysqli_num_rows($check_credentials)>=1)
   //    {
   //       $delete_query = mysqli_query($connection_string,"DELETE FROM users WHERE id = '$row_id'");
   //       if(mysqli_affected_rows($connection_string)>=1){
   //          $response["error"]   = false;
   //          $response["message"]= "Successfully deleted";
   //          $response["status"]= "Success";
   //       }
   //       else{
   //          $response["error"]   = true;
   //          $response["message"]= "Errors";
   //          $response["status"]= "Fail";
   //       }
   //    }
   //    else{
   //       $response["error"]= true;
   //       $response["message"]= "No Record Found";
   //       $response["status"]  = "Fail";
   //    }
   //    $json_response = json_encode($response);
   //    return $json_response;
   // }
   public function update($new_data,$connection_string){
      $response = [];
      $name = $new_data["name"];
      $id = $new_data["id"];
      $email = $new_data["email"];
      $update_rows = mysqli_query($connection_string,"UPDATE users SET name = '$name' , email='$email' WHERE id = '$id'");
      if(mysqli_affected_rows($connection_string)>=1){
         $response["error"]   = false;
         $response["message"] = "Successfully Updated";
         $response["status"]= "Success";
      }
      else{
         $response["error"]   = true;
         $response["message"] = "Error";
         $response["status"]= "Fail";
      }
      $json_response = json_encode($response);
      return $json_response;
   }
}
?>