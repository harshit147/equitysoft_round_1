<?php


/**
 * POST Request 
 */
require("Connection.php");
require("Main.php");
$db_connection = Connection::connect();      // connection to database
$API = new Main();
$credentails = array();
if(isset($_POST["operation"]))      // check the operation parameter in JSON request
{
   $operation = strtolower($_SERVER["operation"]);
   if($operation == "login"){
      $credentails["username"] = strtolower($_POST["username"]);
      $credentails["password"] = strtolower($_POST["password"]);
      $login_API = $API->login($credentails,$db_connection);
      if(isset($login_API) && ! empty($login_API)){
         echo $login_API;
      }
   }
   elseif($operation == "signup"){
      $credentails["full_name"]  = strtolower($_POST["full_name"]);
      $credentails["username"]   = strtolower($_POST["username"]);
      $credentails["password"]   = strtolower($_POST["password"]);
      $signUp_API = $API->signup($credentails,$db_connection);
      if(isset($signUp_API) && ! empty($signUp_API))
      {
         echo $signUp_API;
      }
   }
   elseif($operation == "delete")            // Here row_id is the API key 
   {
      $row_id = $_SERVER["id"];
      $post = [
         "id"=>$row_id,
      ];
      $client = curl_init("http://localhost/Pratice_Example/Equity_API/delete.php");
      curl_setopt($client,CURLOPT_HTTPPOST,1);
      curl_setopt($client,CURLOPT_POSTFIELDS,$post);
      curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
      $response = curl_exec($client);
      curl_close($client);
      $json_response=json_decode($response,true);
      echo $json_response;
   }
   elseif($operation == "update"){
      $credentails["id"] = $_SERVER["id"];
      $credentails["name"] = $_SERVER["name"];
      $credentails["email"] = $_SERVER["email"];
      $update = $API->update($credentails,$db_connection);
      echo $update;
   }
   elseif($operation == "list"){
      $curl = curl_init("http://localhost/Pratice_Example\Equity_API\list.php");
      curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
      $response = curl_exec($curl);
      echo $response;
   }
}


// Headers are compulsory to send 
                  // IMORTANT //
/**\
 * OR instead of creating classes in Main.php
 * 
 * we can create sperate file of every action
 * BUT we can use those file by using the
 * $ch = curl_init(link);  EX : http://localhost/update.php?key=12;
 * curl_exec($ch)
 */
?>